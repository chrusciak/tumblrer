package com.lightsoft.tumblrviewer.network.model.post;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mateusz on 2017-02-12.
 */

public class VideoPost extends TumblrPost {

    @SerializedName("video-caption")
    public String videoCaption;

    @SerializedName("video-sources")
    public String videoSource;

    @SerializedName("video-player-250")
    public String videoPlayer250;

    @SerializedName("video-player-500")
    public String videoPlayer500;
}
