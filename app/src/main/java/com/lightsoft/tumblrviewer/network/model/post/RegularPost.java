package com.lightsoft.tumblrviewer.network.model.post;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mateusz on 2017-02-12.
 */

public class RegularPost extends TumblrPost {

    @SerializedName("regular-title")
    public String title;

    @SerializedName("regular-body")
    public String body;
}
