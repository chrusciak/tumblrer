package com.lightsoft.tumblrviewer.network.model.post;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mateusz on 2017-02-12.
 */

public class AudioPost extends TumblrPost {
    @SerializedName("audio-caption")
    public String audioCaption;

    @SerializedName("id3-title")
    public String audioTitle;
}
