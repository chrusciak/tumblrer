package com.lightsoft.tumblrviewer.network.model.post;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mateusz on 2017-02-12.
 */

public class PhotoPost extends TumblrPost {

    @SerializedName("photo-caption")
    public String photoCaption;

    @SerializedName("photo-url-1280")
    public String photoUrl;
}
