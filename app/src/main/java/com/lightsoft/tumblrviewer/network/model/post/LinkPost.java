package com.lightsoft.tumblrviewer.network.model.post;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mateusz on 2017-02-12.
 */

public class LinkPost extends TumblrPost {

    @SerializedName("link-description")
    public String linkDescription;

    @SerializedName("link-url")
    public String linkUrl;
}
