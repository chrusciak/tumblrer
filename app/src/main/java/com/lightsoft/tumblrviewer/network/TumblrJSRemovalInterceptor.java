package com.lightsoft.tumblrviewer.network;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by Mateusz on 2017-02-08.
 */

public class TumblrJSRemovalInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {

        Response response = chain.proceed(chain.request());
        ResponseBody originalBody = response.body();
        MediaType contentType = originalBody.contentType();

        String json = removeJSVariableFromResponse(originalBody);
        ResponseBody responseBody = ResponseBody.create(contentType, json);

        return response.newBuilder()
                       .body(responseBody)
                       .build();
    }

    protected String removeJSVariableFromResponse(ResponseBody responseBody) throws IOException {
        String json = responseBody.string();
        json = json.replaceAll("var\\s*tumblr_api_read\\s*=\\s*", "")
                   .replaceAll("\\s*;\\s*$", "");
        return json;
    }
}
