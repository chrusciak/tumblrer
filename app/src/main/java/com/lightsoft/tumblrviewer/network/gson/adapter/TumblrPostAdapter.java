package com.lightsoft.tumblrviewer.network.gson.adapter;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.lightsoft.tumblrviewer.network.model.PostType;
import com.lightsoft.tumblrviewer.network.model.post.TumblrPost;

import java.lang.reflect.Type;

import javax.inject.Inject;

/**
 * Created by Mateusz on 2017-02-12.
 */

public class TumblrPostAdapter implements JsonSerializer<TumblrPost>, JsonDeserializer<TumblrPost> {

    @Inject
    public TumblrPostAdapter() {
    }

    @Override
    public TumblrPost deserialize(JsonElement json, Type typeOfT,
            JsonDeserializationContext context) throws JsonParseException {
        JsonElement typeElement = json.getAsJsonObject()
                .get("type");
        try {
            return deserializeTumblrPost(json, context, typeElement);
        } catch (Exception e) {
            return null;
        }
    }

    private TumblrPost deserializeTumblrPost(JsonElement json, JsonDeserializationContext context,
            JsonElement typeElement) {
        Class objectType = PostType.valueOf(typeElement.getAsString())
                .getModelClass();
        return context.deserialize(json, objectType);
    }

    @Override
    public JsonElement serialize(TumblrPost src, Type typeOfSrc, JsonSerializationContext context) {
        return context.serialize(src);
    }
}
