package com.lightsoft.tumblrviewer.network.model;

import com.google.gson.annotations.SerializedName;
import com.lightsoft.tumblrviewer.network.model.post.TumblrPost;

import java.util.List;

/**
 * Created by Mateusz on 2017-02-12.
 */

public class ApiRead
{
    @SerializedName("posts-total")
    public int totalPosts;

    @SerializedName("posts")
    public List<TumblrPost> posts;
}
