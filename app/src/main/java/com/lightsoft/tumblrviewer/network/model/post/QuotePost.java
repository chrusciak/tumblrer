package com.lightsoft.tumblrviewer.network.model.post;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mateusz on 2017-02-12.
 */

public class QuotePost extends TumblrPost {

    @SerializedName("quote-text")
    public String quoteText;

    @SerializedName("quote-source")
    public String quoteSource;
}
