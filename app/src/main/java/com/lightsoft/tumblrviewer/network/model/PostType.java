package com.lightsoft.tumblrviewer.network.model;

import com.lightsoft.tumblrviewer.network.model.post.AnswerPost;
import com.lightsoft.tumblrviewer.network.model.post.AudioPost;
import com.lightsoft.tumblrviewer.network.model.post.ConversationPost;
import com.lightsoft.tumblrviewer.network.model.post.LinkPost;
import com.lightsoft.tumblrviewer.network.model.post.PhotoPost;
import com.lightsoft.tumblrviewer.network.model.post.QuotePost;
import com.lightsoft.tumblrviewer.network.model.post.RegularPost;
import com.lightsoft.tumblrviewer.network.model.post.VideoPost;

/**
 * Created by Mateusz on 2017-02-08.
 */

public enum PostType {
    regular(RegularPost.class),
    photo(PhotoPost.class),
    quote(QuotePost.class),
    link(LinkPost.class),
    conversation(ConversationPost.class),
    audio(AudioPost.class),
    video(VideoPost.class),
    answer(AnswerPost.class);

    private Class modelClass;

    PostType(Class modelClass) {
        this.modelClass = modelClass;
    }

    public Class getModelClass() {
        return modelClass;
    }
}
