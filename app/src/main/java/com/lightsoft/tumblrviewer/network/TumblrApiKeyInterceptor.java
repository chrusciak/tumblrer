package com.lightsoft.tumblrviewer.network;

import android.support.annotation.NonNull;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Mateusz on 2017-02-08.
 */

public class TumblrApiKeyInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {

        Request origRequest = chain.request();
        HttpUrl newUrl = createUrlWithApiKey(origRequest);
        Request request = createNewRequest(origRequest, newUrl);
        return chain.proceed(request);
    }

    private Request createNewRequest(Request origRequest, HttpUrl newUrl) {
        Request.Builder requestBuilder = origRequest.newBuilder()
                                                    .url(newUrl);
        return requestBuilder.build();
    }

    @NonNull
    private HttpUrl createUrlWithApiKey(Request origRequest) {
        HttpUrl origUrl = origRequest.url();

        return origUrl.newBuilder()
                      .addQueryParameter("api_key",
                                         "fuiKNFp9vQFvjLNvx4sUwti4Yb5yGutBN4Xh10LXZhhRKjWlV4")
                      .build();
    }
}
