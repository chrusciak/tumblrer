package com.lightsoft.tumblrviewer.network;

import com.lightsoft.tumblrviewer.network.model.ApiRead;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Mateusz on 2017-02-08.
 */

public interface TumblrService {
    @GET("api/read/json")
    Call<ApiRead> getPosts(@Query("start") int offset, @Query("num") int
    downloadItem);
}
