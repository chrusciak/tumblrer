package com.lightsoft.tumblrviewer.network.model.post;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mateusz on 2017-02-12.
 */

public class AnswerPost extends TumblrPost {
    @SerializedName("question")
    public String question;

    @SerializedName("answer")
    public String answer;


}
