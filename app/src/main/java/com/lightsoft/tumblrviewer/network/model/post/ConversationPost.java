package com.lightsoft.tumblrviewer.network.model.post;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mateusz on 2017-02-12.
 */

public class ConversationPost extends TumblrPost {
    @SerializedName("conversation-title")
    public String title;

    @SerializedName("conversation")
    public List<Phrase> conversation;
}
