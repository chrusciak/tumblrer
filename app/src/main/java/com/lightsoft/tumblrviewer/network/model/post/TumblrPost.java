package com.lightsoft.tumblrviewer.network.model.post;

import com.google.gson.annotations.SerializedName;
import com.lightsoft.tumblrviewer.network.model.PostType;

/**
 * Created by Mateusz on 2017-02-08.
 */

public class TumblrPost {
    @SerializedName("url")
    public String url;

    @SerializedName("unix-timestamp")
    public long timestamp;

    @SerializedName("type")
    public PostType postType;
}