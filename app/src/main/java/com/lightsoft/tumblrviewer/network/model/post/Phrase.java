package com.lightsoft.tumblrviewer.network.model.post;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mateusz on 2017-02-13.
 */

public class Phrase {
    @SerializedName("label")
    public String label;

    @SerializedName("name")
    public String name;

    @SerializedName("phrase")
    public String phrase;
}
