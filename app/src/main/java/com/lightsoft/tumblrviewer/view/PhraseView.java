package com.lightsoft.tumblrviewer.view;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.lightsoft.tumblrviewer.R;
import com.lightsoft.tumblrviewer.network.model.post.Phrase;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mateusz on 2017-02-13.
 */

public class PhraseView extends FrameLayout {

    @BindView(R.id.personName)
    TextView personName;

    @BindView(R.id.personPhrase)
    TextView personPhrase;

    public PhraseView(Context context) {
        super(context);
        init();
    }

    public PhraseView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PhraseView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public PhraseView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void init() {
        LayoutInflater.from(getContext())
                .inflate(R.layout.view_chat_item, this);
        ButterKnife.bind(this);
    }

    public void setPersonName(String name)
    {
        String formattedName = getResources().getString(R.string.conversation_name_format, name);
        personName.setText(formattedName);
    }

    public void setPhraseText(String phraseText)
    {
        personPhrase.setText(phraseText);
    }
}
