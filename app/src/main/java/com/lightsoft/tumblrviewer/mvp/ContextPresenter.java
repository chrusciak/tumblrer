package com.lightsoft.tumblrviewer.mvp;

public class ContextPresenter<T extends ContextView> implements Presenter {

    private T contextView;

    public ContextPresenter(T contextView) {
        this.contextView = contextView;
    }

    public T getView() {
        return contextView;
    }

    public void onDestroy() {
        contextView = null;
    }
}
