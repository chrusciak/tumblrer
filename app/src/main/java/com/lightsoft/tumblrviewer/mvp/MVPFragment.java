package com.lightsoft.tumblrviewer.mvp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class MVPFragment<P extends ContextPresenter<?>> extends Fragment {

    private Unbinder unbinder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        initializeComponent();
        super.onCreate(savedInstanceState);
    }

    public abstract void initializeComponent();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(onMainLayout(), null);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    /**
     * @return layout res id for fragment
     */
    public abstract int onMainLayout();

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        getPresenter().onDestroy();
        super.onDestroy();
    }

    public abstract P getPresenter();
}
