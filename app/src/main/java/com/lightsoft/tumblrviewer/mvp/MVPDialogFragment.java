package com.lightsoft.tumblrviewer.mvp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;

public abstract class MVPDialogFragment<T extends ContextPresenter> extends DialogFragment {
    private T contextPresenter;

    public MVPDialogFragment() {
        initializeDagger();
        contextPresenter = createPresenter();
    }

    /**
     * Method to create and setup presenter for current activity
     *
     * @return
     * @deprecated use initializeDagger() and dependency injection instead of this method
     */
    @Deprecated
    public abstract T createPresenter();

    public abstract void initializeDagger();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public T getPresenter() {
        return contextPresenter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = createView(inflater, container);
        ButterKnife.bind(this, view);
        return view;
    }

    public abstract int onMainLayout();

    @Override
    public void onDestroy() {
        contextPresenter.onDestroy();
        contextPresenter = null;
        super.onDestroy();
    }

    @Override
    public Context getContext() {
        Context context = super.getContext();
        if (context == null) {
            Fragment fragment = getTargetFragment();
            if (fragment != null) return fragment.getContext();
        }
        return context;
    }

    public View createView(LayoutInflater inflater, @Nullable ViewGroup container) {
        return inflater.inflate(onMainLayout(), container, false);
    }
}
