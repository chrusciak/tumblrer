package com.lightsoft.tumblrviewer.mvp;

import android.content.Context;

public interface ContextView {
    Context getContext();
}
