package com.lightsoft.tumblrviewer.mvp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

public abstract class MVPActivity<T extends ContextView> extends AppCompatActivity {

    private ContextPresenter<T> contextPresenter;

    /**
     * Method to create and setup presenter for current activity
     *
     * @return
     */
    public abstract ContextPresenter<T> createPresenter();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contextPresenter = createPresenter();
    }

    @Override
    protected void onDestroy() {
        contextPresenter.onDestroy();
        contextPresenter = null;
        super.onDestroy();
    }


    public Context getContext() {
        return getBaseContext();
    }
}
