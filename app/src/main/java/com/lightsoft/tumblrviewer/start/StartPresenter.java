package com.lightsoft.tumblrviewer.start;

import com.lightsoft.tumblrviewer.mvp.ContextPresenter;

import org.jsoup.helper.StringUtil;

import javax.inject.Inject;

/**
 * Created by Mateusz on 2017-02-15.
 */

public class StartPresenter extends ContextPresenter<StartView> {
    @Inject
    public StartPresenter(StartView contextView) {
        super(contextView);
    }

    public void onGoButtonClicked(String username) {
        if(!StringUtil.isBlank(username)) {
            getView().openBlogForUser(username);
            getView().closeSoftKeyboard();
        }
    }
}
