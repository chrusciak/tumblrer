package com.lightsoft.tumblrviewer.start.component;

import com.lightsoft.tumblrviewer.posts.component.PostsModule;
import com.lightsoft.tumblrviewer.start.StartFragment;
import com.lightsoft.tumblrviewer.start.StartView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mateusz on 2017-02-15.
 */
@Module
public class StartScreenModule {
    private final StartView startView;

    public StartScreenModule(StartFragment startFragment) {
        this.startView = startFragment;
    }

    @Provides
    public StartView startView() {
        return startView;
    }

}
