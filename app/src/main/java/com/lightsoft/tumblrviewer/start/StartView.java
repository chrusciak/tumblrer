package com.lightsoft.tumblrviewer.start;

import com.lightsoft.tumblrviewer.mvp.ContextView;

/**
 * Created by Mateusz on 2017-02-15.
 */

public interface StartView extends ContextView {
    void openBlogForUser(String username);

    void closeSoftKeyboard();
}
