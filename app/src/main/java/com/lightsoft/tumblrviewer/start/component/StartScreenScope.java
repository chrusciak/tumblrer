package com.lightsoft.tumblrviewer.start.component;

import javax.inject.Scope;

/**
 * Created by Mateusz on 2017-02-15.
 */
@Scope
public @interface StartScreenScope {
}
