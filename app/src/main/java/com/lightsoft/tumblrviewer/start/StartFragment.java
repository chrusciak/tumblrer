package com.lightsoft.tumblrviewer.start;

import android.content.Context;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.lightsoft.tumblrviewer.R;
import com.lightsoft.tumblrviewer.mvp.MVPFragment;
import com.lightsoft.tumblrviewer.posts.PostsFragment;
import com.lightsoft.tumblrviewer.start.component.DaggerStartScreenComponent;
import com.lightsoft.tumblrviewer.start.component.StartScreenComponent;
import com.lightsoft.tumblrviewer.start.component.StartScreenModule;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Mateusz on 2017-02-15.
 */

public class StartFragment extends MVPFragment<StartPresenter> implements StartView {

    public static StartFragment createInstance() {
        return new StartFragment();
    }

    @BindView(R.id.usernameEdit)
    EditText usernameEdit;

    @Inject
    StartPresenter startPresenter;


    @Override
    public void initializeComponent() {
        StartScreenComponent startScreenComponent = DaggerStartScreenComponent.builder()
                .startScreenModule(new StartScreenModule(this))
                .build();
        startScreenComponent.injectStartFragment(this);
    }

    @Override
    public int onMainLayout() {
        return R.layout.fragment_start;
    }

    @OnClick(R.id.goButton)
    public void onGoButtonClicked() {
        getPresenter().onGoButtonClicked(usernameEdit.getText()
                                                 .toString());
    }

    @Override
    public StartPresenter getPresenter() {
        return startPresenter;
    }

    @Override
    public void openBlogForUser(String username) {
        getFragmentManager().beginTransaction()
                .replace(R.id.content, PostsFragment.createInstance(username))
                .addToBackStack("")
                .commit();
    }

    @Override
    public void closeSoftKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }
}
