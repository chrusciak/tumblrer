package com.lightsoft.tumblrviewer.start.component;

import com.lightsoft.tumblrviewer.start.StartFragment;

import dagger.Component;

/**
 * Created by Mateusz on 2017-02-15.
 */
@Component(modules = StartScreenModule.class)
@StartScreenScope
public interface StartScreenComponent {
    void injectStartFragment(StartFragment startFragment);
}
