package com.lightsoft.tumblrviewer.posts.holder;

import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.TextView;

import com.lightsoft.tumblrviewer.R;
import com.lightsoft.tumblrviewer.network.model.post.LinkPost;
import com.lightsoft.tumblrviewer.posts.HtmlTagRemover;

import butterknife.BindView;

/**
 * Created by Mateusz on 2017-02-10.
 */

public class LinkViewHolder extends PostViewHolder<LinkPost> {

    @BindView(R.id.link)
    TextView linkView;

    @BindView(R.id.linkCaption)
    TextView linkTextView;

    public LinkViewHolder(View itemView) {
        super(itemView);
        linkView.setMovementMethod(LinkMovementMethod.getInstance());
        linkTextView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void bindView(LinkPost item) {
        super.bindView(item);
        String linkText = new HtmlTagRemover(item.linkDescription).toCleanString();
        linkView.setText(item.linkUrl);
        linkTextView.setText(linkText);
    }
}
