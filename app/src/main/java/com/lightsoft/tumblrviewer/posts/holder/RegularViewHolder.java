package com.lightsoft.tumblrviewer.posts.holder;

import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.lightsoft.tumblrviewer.R;
import com.lightsoft.tumblrviewer.network.model.post.RegularPost;
import com.lightsoft.tumblrviewer.posts.HtmlTagRemover;

import butterknife.BindView;

/**
 * Created by Mateusz on 2017-02-10.
 */

public class RegularViewHolder extends PostViewHolder<RegularPost> {

    @BindView(R.id.postTitle)
    TextView titleView;

    @BindView(R.id.postBody)
    TextView postBody;

    public RegularViewHolder(View itemView) {
        super(itemView);
    }

    public void bindView(RegularPost item) {
        super.bindView(item);
        String bodyText = new HtmlTagRemover(item.body).toCleanString();
        titleView.setText(item.title);
        postBody.setText(bodyText);
    }
}
