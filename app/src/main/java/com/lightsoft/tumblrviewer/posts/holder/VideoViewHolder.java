package com.lightsoft.tumblrviewer.posts.holder;

import android.graphics.Bitmap;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.lightsoft.tumblrviewer.R;
import com.lightsoft.tumblrviewer.network.model.post.VideoPost;
import com.lightsoft.tumblrviewer.posts.HtmlTagRemover;

import butterknife.BindView;

/**
 * Created by Mateusz on 2017-02-10.
 */

public class VideoViewHolder extends PostViewHolder<VideoPost> {


    @BindView(R.id.postTitle)
    TextView textView;

    @BindView(R.id.videoIframe)
    WebView videoIframe;

    @BindView(R.id.videoPlaceholder)
    ImageView videoPlaceholder;

    public VideoViewHolder(View itemView) {
        super(itemView);
        WebSettings webSettings = videoIframe.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setLoadWithOverviewMode(true);
        videoIframe.setScrollbarFadingEnabled(false);
        videoIframe.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                hidePlaceholder();
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                showPlaceholder();
            }
        });
        videoIframe.setInitialScale((int) (720 / 255.0 * 100));
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void bindView(final VideoPost item) {
        super.bindView(item);
        showPlaceholder();
        String videoCaption = new HtmlTagRemover(item.videoCaption).toCleanString();
        textView.setText(videoCaption);
        videoIframe.loadData(item.videoPlayer250, "text/html", null);
    }

    private void showPlaceholder() {
        videoPlaceholder.setVisibility(View.VISIBLE);
    }

    private void hidePlaceholder() {
        videoPlaceholder.setVisibility(View.GONE);
    }


}
