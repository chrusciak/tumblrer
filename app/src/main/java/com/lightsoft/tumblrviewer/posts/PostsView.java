package com.lightsoft.tumblrviewer.posts;

import com.lightsoft.tumblrviewer.mvp.ContextView;
import com.lightsoft.tumblrviewer.network.model.post.TumblrPost;

import java.util.List;

/**
 * Created by Mateusz on 2017-02-08.
 */

public interface PostsView extends ContextView {
    void addData(List<TumblrPost> posts);

    void disableLoadingIndicators();

    void clearList();

    void setMaximumListItems(int i);

    void showConnectionProblemMessage();

    String getUsername();

    void showPageNotFound();
}
