package com.lightsoft.tumblrviewer.posts.component;

import com.lightsoft.tumblrviewer.application.component.TumblrAppComponent;
import com.lightsoft.tumblrviewer.posts.PostsFragment;
import com.lightsoft.tumblrviewer.posts.PostsView;

import dagger.Component;

/**
 * Created by Mateusz on 2017-02-08.
 */
@PostsScope
@Component(modules = CustomTabsModule.class, dependencies = TumblrAppComponent.class)
public interface PostsComponent {
    void injectPostsFragment(PostsFragment postsFragment);
}
