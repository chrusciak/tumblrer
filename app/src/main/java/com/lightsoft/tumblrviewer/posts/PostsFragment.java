package com.lightsoft.tumblrviewer.posts;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.lightsoft.tumblrviewer.R;
import com.lightsoft.tumblrviewer.application.TumblrApplication;
import com.lightsoft.tumblrviewer.application.component.TumblrAppComponent;
import com.lightsoft.tumblrviewer.mvp.MVPFragment;
import com.lightsoft.tumblrviewer.network.model.post.TumblrPost;
import com.lightsoft.tumblrviewer.posts.component.DaggerPostsComponent;
import com.lightsoft.tumblrviewer.posts.component.PostsComponent;
import com.lightsoft.tumblrviewer.posts.component.PostsModule;

import java.util.List;


import javax.inject.Inject;

import butterknife.BindView;


/**
 * Created by Mateusz on 2017-02-08.
 */

public class PostsFragment extends MVPFragment<PostsPresenter> implements PostsView,
        SwipeRefreshLayout.OnRefreshListener {

    private static final String KEY_USERNAME = "username";

    @Inject
    PostsPresenter postsPresenter;

    @BindView(R.id.postsRecycler)
    protected RecyclerView postsRecycler;

    @BindView(R.id.swipeRefreshLayout)
    protected SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.notFoundLayout)
    protected View notFoundLayout;

    @Inject
    PostsAdapter postsAdapter;

    ListScrollPaginator listScrollPaginator;

    @Override
    public void initializeComponent() {

        TumblrAppComponent tumblrAppComponent = getTumblrAppComponent();

        PostsComponent postsComponent = DaggerPostsComponent.builder()
                .tumblrAppComponent(tumblrAppComponent)
                .postsModule(new PostsModule(this))
                .build();
        postsComponent.injectPostsFragment(this);
    }

    private TumblrAppComponent getTumblrAppComponent() {
        return TumblrApplication.getApplication(getActivity())
                .getTumblrAppComponent();
    }

    @Override
    public int onMainLayout() {
        return R.layout.fragment_posts;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpRecyclerView();
        setUpSwipeRefreshLayout();
        getPresenter().onViewCreated();
    }

    private void setUpSwipeRefreshLayout() {
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    private void setUpRecyclerView() {
        postsRecycler.setAdapter(postsAdapter);
        listScrollPaginator = new ListScrollPaginator(-1);
        postsRecycler.addOnScrollListener(listScrollPaginator);
        postsRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public PostsPresenter getPresenter() {
        return postsPresenter;
    }

    public static PostsFragment createInstance(String tumblrUsername) {
        Bundle args = new Bundle();
        args.putString(KEY_USERNAME, tumblrUsername);
        PostsFragment postsFragment = new PostsFragment();
        postsFragment.setArguments(args);
        return postsFragment;
    }

    @Override
    public void addData(List<TumblrPost> posts) {
        postsAdapter.addItems(posts);
        postsAdapter.notifyDataSetChanged();
    }

    @Override
    public void disableLoadingIndicators() {
        swipeRefreshLayout.setRefreshing(false);
        listScrollPaginator.setLoading(false);
        postsAdapter.setLoadingVisible(false);
    }

    @Override
    public void clearList() {
        postsAdapter.clear();
        postsAdapter.notifyDataSetChanged();
    }

    @Override
    public void setMaximumListItems(int maxItems) {
        listScrollPaginator.setMaximumItems(maxItems);
    }

    @Override
    public void showConnectionProblemMessage() {
        Toast.makeText(getContext(), R.string.connection_problem_error_message, Toast.LENGTH_LONG);
    }

    @Override
    public String getUsername() {
        return getArguments().getString(KEY_USERNAME);
    }

    @Override
    public void showPageNotFound() {
        notFoundLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRefresh() {
        getPresenter().onListRefreshed();
    }

    @Override
    public void onDestroyView() {
        getPresenter().onViewDestroyed();
        super.onDestroyView();
    }

    public class ListScrollPaginator extends RecyclerView.OnScrollListener {

        boolean isLoading = false;

        public static final int VISIBLE_THRESHOLD = 3;

        private int maxItems = -1;

        /**
         * @param maxItems max items for pagination, pass -1 to infinity pages
         */
        public ListScrollPaginator(int maxItems) {
            this.maxItems = maxItems;
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (isLoading)
                return;

            RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
            if (!(layoutManager instanceof LinearLayoutManager))
                return;
            LinearLayoutManager linearLayoutManager = (LinearLayoutManager) layoutManager;
            int itemCount = linearLayoutManager.getItemCount();
            int lastVisibleItemPosition = linearLayoutManager.findLastVisibleItemPosition();
            if (itemCount <= (lastVisibleItemPosition + VISIBLE_THRESHOLD)
                    && !isExceedMaxItems(itemCount)) {
                isLoading = true;
                onListOverScroll(lastVisibleItemPosition);
            }
        }

        private boolean isExceedMaxItems(int itemCount) {
            return maxItems != -1 && itemCount >= maxItems;
        }

        public void setLoading(boolean loading) {
            isLoading = loading;
        }

        public void setMaximumItems(int maximumItems) {
            this.maxItems = maximumItems;
        }
    }

    private void onListOverScroll(int lastVisibleItemPosition) {
        postsAdapter.setLoadingVisible(true);
        getPresenter().onListOverScroll(lastVisibleItemPosition);
    }

}
