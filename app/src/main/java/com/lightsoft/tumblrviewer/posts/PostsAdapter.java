package com.lightsoft.tumblrviewer.posts;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lightsoft.tumblrviewer.R;
import com.lightsoft.tumblrviewer.list.RecyclerAdapter;
import com.lightsoft.tumblrviewer.list.ViewHolder;
import com.lightsoft.tumblrviewer.network.model.PostType;
import com.lightsoft.tumblrviewer.network.model.post.TumblrPost;
import com.lightsoft.tumblrviewer.posts.holder.AnswerViewHolder;
import com.lightsoft.tumblrviewer.posts.holder.AudioViewHolder;
import com.lightsoft.tumblrviewer.posts.holder.ConversationViewHolder;
import com.lightsoft.tumblrviewer.posts.holder.LinkViewHolder;
import com.lightsoft.tumblrviewer.posts.holder.PhotoViewHolder;
import com.lightsoft.tumblrviewer.posts.holder.PostViewHolder;
import com.lightsoft.tumblrviewer.posts.holder.QuoteViewHolder;
import com.lightsoft.tumblrviewer.posts.holder.RegularViewHolder;
import com.lightsoft.tumblrviewer.posts.holder.VideoViewHolder;

import javax.inject.Inject;

/**
 * Created by Mateusz on 2017-02-10.
 */

public class PostsAdapter extends RecyclerAdapter<TumblrPost> {

    @Inject
    OnOpenPostListener onOpenPostListener;

    @Inject
    public PostsAdapter() {

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder = super.onCreateViewHolder(parent, viewType);
        if (viewHolder == null && viewType > 0) {
            viewHolder = createPostsHolder(parent, viewType);
        }
        return viewHolder;
    }


    @Override
    public int getItemViewType(int position) {
        if (position < getDataCount()) {
            TumblrPost post = getItem(position);
            return post.postType.ordinal() + 1;
        }
        return super.getItemViewType(position);
    }

    @NonNull
    private ViewHolder createPostsHolder(ViewGroup parent, int viewType) {
        PostType postType = PostType.values()[viewType - 1];
        PostViewHolder viewHolder = null;
        if (postType == PostType.video) {
            View videoView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_video_post, parent, false);
            viewHolder = new VideoViewHolder(videoView);
        } else if (postType == PostType.regular) {
            View regularView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_regular_post, parent, false);
            viewHolder = new RegularViewHolder(regularView);
        } else if (postType == PostType.answer) {
            View audioView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_answer_post, parent, false);
            viewHolder = new AnswerViewHolder(audioView);
        } else if (postType == PostType.audio) {
            View videoView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_audio_post, parent, false);
            viewHolder = new AudioViewHolder(videoView);
        } else if (postType == PostType.quote) {
            View videoView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_quote_post, parent, false);
            viewHolder = new QuoteViewHolder(videoView);
        } else if (postType == PostType.conversation) {
            View conversationView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_conversation_post, parent, false);
            viewHolder = new ConversationViewHolder(conversationView);
        } else if (postType == PostType.photo) {
            View photoView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_photo_post, parent, false);
            viewHolder = new PhotoViewHolder(photoView);
        } else if (postType == PostType.link) {
            View linkView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_link_post, parent, false);
            viewHolder = new LinkViewHolder(linkView);
        }
        if(viewHolder != null) viewHolder.setOnPostOpenListener(onOpenPostListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (holder instanceof PostViewHolder) {
            PostViewHolder postViewHolder = (PostViewHolder) holder;
            postViewHolder.bindView(getItem(position));
        }

    }
}
