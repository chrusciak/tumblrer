package com.lightsoft.tumblrviewer.posts.holder;

import android.view.View;
import android.widget.TextView;

import com.lightsoft.tumblrviewer.R;
import com.lightsoft.tumblrviewer.network.model.post.AnswerPost;
import com.lightsoft.tumblrviewer.network.model.post.RegularPost;
import com.lightsoft.tumblrviewer.posts.HtmlTagRemover;

import butterknife.BindView;

/**
 * Created by Mateusz on 2017-02-10.
 */

public class AnswerViewHolder extends PostViewHolder<AnswerPost> {

    @BindView(R.id.questionText)
    TextView questionText;

    @BindView(R.id.answerText)
    TextView answerText;

    public AnswerViewHolder(View itemView) {
        super(itemView);
    }

    public void bindView(AnswerPost item) {
        super.bindView(item);
        String answer = new HtmlTagRemover(item.answer).toCleanString();
        String question = new HtmlTagRemover(item.question).toCleanString();
        questionText.setText(question);
        answerText.setText(answer);
    }
}
