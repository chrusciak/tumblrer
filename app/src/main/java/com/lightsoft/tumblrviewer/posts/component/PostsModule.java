package com.lightsoft.tumblrviewer.posts.component;

import android.content.Context;

import com.google.gson.Gson;
import com.lightsoft.tumblrviewer.application.component.TumblrApplicationScope;
import com.lightsoft.tumblrviewer.network.TumblrService;
import com.lightsoft.tumblrviewer.posts.PostsDataProvider;
import com.lightsoft.tumblrviewer.posts.PostsFragment;
import com.lightsoft.tumblrviewer.posts.PostsView;
import com.lightsoft.tumblrviewer.posts.ServicePostsDataProvider;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Mateusz on 2017-02-08.
 */
@Module
public class PostsModule {
    private final PostsView postsView;

    public PostsModule(PostsFragment postFragment) {
        this.postsView = postFragment;
    }

    @Provides
    @PostsScope
    public PostsView postView() {
        return postsView;
    }


    @Provides
    @PostsScope
    public Context context()
    {
        return postsView.getContext();
    }

    @Provides
    @PostsScope
    public PostsDataProvider postsDataProvider(TumblrService tumblrService) {
        return new ServicePostsDataProvider(tumblrService);
    }
    @Provides
    @PostsScope
    TumblrService tumblrService(OkHttpClient okHttpClient, Gson gson) {
        GsonConverterFactory converterFactory = GsonConverterFactory.create(gson);
        Retrofit tumblrRetrofit = new Retrofit.Builder().addConverterFactory(converterFactory)
                .client(okHttpClient)
                .baseUrl(createBaseUrl(postsView.getUsername()))
                .build();
        return tumblrRetrofit.create(TumblrService.class);
    }

    private String createBaseUrl(String username)
    {
        return "http://www." + username + ".tumblr.com/";
    }

}
