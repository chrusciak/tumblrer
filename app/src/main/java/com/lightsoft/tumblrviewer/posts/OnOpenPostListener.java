package com.lightsoft.tumblrviewer.posts;

/**
 * Created by Mateusz on 2017-02-14.
 */

public interface OnOpenPostListener {
    void onOpenPost(String url);
}
