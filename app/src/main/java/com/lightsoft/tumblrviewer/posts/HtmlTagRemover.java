package com.lightsoft.tumblrviewer.posts;

import org.jsoup.Jsoup;

/**
 * Created by Mateusz on 2017-02-12.
 */

public class HtmlTagRemover {

    private String html;

    public HtmlTagRemover(String html) {
        this.html = html;
    }

    public String toCleanString() {
        return Jsoup.parse(html)
                .text();
    }
}
