package com.lightsoft.tumblrviewer.posts.holder;

import android.view.View;
import android.widget.TextView;

import com.lightsoft.tumblrviewer.R;
import com.lightsoft.tumblrviewer.network.model.post.QuotePost;
import com.lightsoft.tumblrviewer.network.model.post.RegularPost;
import com.lightsoft.tumblrviewer.posts.HtmlTagRemover;

import butterknife.BindView;

/**
 * Created by Mateusz on 2017-02-10.
 */

public class QuoteViewHolder extends PostViewHolder<QuotePost> {

    @BindView(R.id.quoteText)
    TextView quoteText;

    @BindView(R.id.quoteSource)
    TextView quoteSource;

    public QuoteViewHolder(View itemView) {
        super(itemView);
    }

    public void bindView(QuotePost item) {
        super.bindView(item);
        String source = new HtmlTagRemover(item.quoteSource).toCleanString();
        String quote = getView().getResources().getString(R.string.quote_format, item.quoteText);
        quoteText.setText(quote);
        quoteSource.setText(source);
    }
}
