package com.lightsoft.tumblrviewer.posts;

import android.util.Log;

import com.lightsoft.tumblrviewer.mvp.ContextPresenter;
import com.lightsoft.tumblrviewer.network.model.ApiRead;
import com.lightsoft.tumblrviewer.network.model.post.TumblrPost;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mateusz on 2017-02-08.
 */

public class PostsPresenter extends ContextPresenter<PostsView> implements Callback<ApiRead> {

    @Inject
    PostsDataProvider postsDataProvider;

    @Inject
    public PostsPresenter(PostsView contextView) {
        super(contextView);
    }

    public void loadMore(int offset) {
        postsDataProvider.loadPosts(offset, this);
    }

    @Override
    public void onResponse(Call<ApiRead> call, Response<ApiRead> response) {
        if (response.code() == 200) {
            ApiRead apiRead = response.body();
            addDataToList(apiRead.posts);
            getView().setMaximumListItems(apiRead.totalPosts);
        } else if (response.code() == 404) {
            getView().showPageNotFound();
        } else {
            getView().showConnectionProblemMessage();
        }
    }

    @Override
    public void onFailure(Call<ApiRead> call, Throwable t) {
        getView().showConnectionProblemMessage();
    }


    private void addDataToList(List<TumblrPost> posts) {
        getView().addData(posts);
        getView().disableLoadingIndicators();
    }

    public void onListOverScroll(int lastElementPosition) {
        loadMore(lastElementPosition);
    }

    public void loadData() {
        postsDataProvider.loadPosts(0, this);
    }

    public void onViewCreated() {
        loadData();
    }

    public void onListRefreshed() {
        getView().clearList();
        loadData();
    }

    public void onViewDestroyed() {
        postsDataProvider.cancelCalls();
    }

}
