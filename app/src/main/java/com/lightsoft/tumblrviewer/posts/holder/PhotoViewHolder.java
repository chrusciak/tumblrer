package com.lightsoft.tumblrviewer.posts.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.lightsoft.tumblrviewer.R;
import com.lightsoft.tumblrviewer.network.model.post.PhotoPost;
import com.lightsoft.tumblrviewer.posts.HtmlTagRemover;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

/**
 * Created by Mateusz on 2017-02-13.
 */

public class PhotoViewHolder extends PostViewHolder<PhotoPost> {

    @BindView(R.id.photo)
    ImageView photoView;

    @BindView(R.id.photoCaption)
    TextView photoCaption;

    public PhotoViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void bindView(PhotoPost item) {
        super.bindView(item);
        Picasso.with(getView().getContext())
                .load(item.photoUrl)
                .placeholder(R.drawable.photo_placeholder)
                .into(photoView);
        String caption = new HtmlTagRemover(item.photoCaption).toCleanString();
        photoCaption.setText(caption);
    }
}
