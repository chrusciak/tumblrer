package com.lightsoft.tumblrviewer.posts;

import com.lightsoft.tumblrviewer.network.TumblrService;
import com.lightsoft.tumblrviewer.network.model.ApiRead;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by Mateusz on 2017-02-08.
 */

public class ServicePostsDataProvider implements PostsDataProvider {

    private int downloadItemCount = 20;

    TumblrService tumblrService;

    private Call<ApiRead> currentCall;

    @Inject
    public ServicePostsDataProvider(TumblrService tumblrService) {
        this.tumblrService = tumblrService;
    }

    @Override
    public void loadPosts(int offset, Callback<ApiRead> callback) {
        if (currentCall != null && !currentCall.isCanceled())
            currentCall.cancel();
        currentCall = tumblrService.getPosts(offset, downloadItemCount);
        currentCall.enqueue(callback);
    }

    @Override
    public void cancelCalls() {
        currentCall.cancel();
    }

}
