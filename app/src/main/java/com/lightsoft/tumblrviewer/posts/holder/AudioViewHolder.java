package com.lightsoft.tumblrviewer.posts.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.lightsoft.tumblrviewer.R;
import com.lightsoft.tumblrviewer.network.model.post.AudioPost;
import com.lightsoft.tumblrviewer.posts.HtmlTagRemover;

import butterknife.BindView;

/**
 * Created by Mateusz on 2017-02-13.
 */

public class AudioViewHolder extends PostViewHolder<AudioPost> {

    @BindView(R.id.audioTitle)
    TextView audioTitle;

    @BindView(R.id.captionText)
    TextView captionText;

    @BindView(R.id.albumCover)
    ImageView coverImage;

    public AudioViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void bindView(AudioPost item) {
        super.bindView(item);
        String caption = new HtmlTagRemover(item.audioCaption).toCleanString();
        audioTitle.setText(item.audioTitle);
        captionText.setText(caption);

    }
}
