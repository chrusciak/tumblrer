package com.lightsoft.tumblrviewer.posts.holder;

import android.view.View;

import com.lightsoft.tumblrviewer.R;
import com.lightsoft.tumblrviewer.list.ViewHolder;
import com.lightsoft.tumblrviewer.network.model.post.TumblrPost;
import com.lightsoft.tumblrviewer.posts.OnOpenPostListener;

import butterknife.BindView;

/**
 * Created by Mateusz on 2017-02-10.
 */

public abstract class PostViewHolder<T extends TumblrPost> extends ViewHolder {

    @BindView(R.id.moreButton)
    View moreButton;

    private T bindedItem;

    public PostViewHolder(View itemView) {
        super(itemView);
        moreButton.setOnClickListener(onMoreButtonClickedListener);
    }

    public void bindView(T item) {
        this.bindedItem = item;
    }

    private OnOpenPostListener onPostOpenListener;

    public void setOnPostOpenListener(OnOpenPostListener onPostOpenListener) {
        this.onPostOpenListener = onPostOpenListener;
    }

    public void dispatchPostOpenEvent(String url) {
        if (this.onPostOpenListener != null) {
            onPostOpenListener.onOpenPost(url);
        }
    }

    View.OnClickListener onMoreButtonClickedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dispatchPostOpenEvent(bindedItem.url);
        }
    };
}

