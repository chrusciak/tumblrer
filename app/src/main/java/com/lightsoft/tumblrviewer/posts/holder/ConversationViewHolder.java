package com.lightsoft.tumblrviewer.posts.holder;

import android.view.View;
import android.widget.LinearLayout;

import com.lightsoft.tumblrviewer.R;
import com.lightsoft.tumblrviewer.network.model.post.ConversationPost;
import com.lightsoft.tumblrviewer.network.model.post.Phrase;
import com.lightsoft.tumblrviewer.view.PhraseView;

import butterknife.BindView;

/**
 * Created by Mateusz on 2017-02-13.
 */

public class ConversationViewHolder extends PostViewHolder<ConversationPost> {

    @BindView(R.id.conversationContainer)
    LinearLayout conversationContainer;

    public ConversationViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void bindView(ConversationPost item) {
        super.bindView(item);
        conversationContainer.removeAllViews();
        for (Phrase phrase : item.conversation) {
            PhraseView phraseView = new PhraseView(getView().getContext());
            phraseView.setPersonName(phrase.name);
            phraseView.setPhraseText(phrase.phrase);
            conversationContainer.addView(phraseView);
        }
    }
}
