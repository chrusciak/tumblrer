package com.lightsoft.tumblrviewer.posts.component;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.support.customtabs.CustomTabsIntent;

import com.lightsoft.tumblrviewer.R;
import com.lightsoft.tumblrviewer.application.component.TumblrApplicationScope;
import com.lightsoft.tumblrviewer.posts.OnOpenPostListener;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mateusz on 2017-02-14.
 */
@Module(includes = PostsModule.class)
@PostsScope
public class CustomTabsModule {
    @Provides
    public CustomTabsIntent.Builder builder(Context context) {
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        int color = getColorFromResources(context);
        builder.setToolbarColor(color);
        return builder;
    }

    @Provides
    public OnOpenPostListener onOpenPostListener(final Context context,
            final CustomTabsIntent.Builder
                    builder) {
        return new OnOpenPostListener() {
            @Override
            public void onOpenPost(String url) {
                builder.build()
                        .launchUrl(context, Uri.parse(url));
            }
        };
    }


    private int getColorFromResources(Context context) {
        int color;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            color = context.getColor(R.color.colorPrimary);
        } else {
            color = context.getResources()
                    .getColor(R.color.colorPrimary);
        }
        return color;
    }
}
