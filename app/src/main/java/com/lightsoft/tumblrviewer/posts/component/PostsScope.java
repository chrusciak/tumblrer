package com.lightsoft.tumblrviewer.posts.component;

import javax.inject.Scope;

/**
 * Created by Mateusz on 2017-02-08.
 */
@Scope
public @interface PostsScope {
}
