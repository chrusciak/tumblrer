package com.lightsoft.tumblrviewer.posts;

import com.lightsoft.tumblrviewer.mvp.DataProvider;
import com.lightsoft.tumblrviewer.network.model.ApiRead;

import retrofit2.Callback;

/**
 * Created by Mateusz on 2017-02-11.
 */
public interface PostsDataProvider extends DataProvider {
    void loadPosts(int offset, Callback<ApiRead> callback);

    void cancelCalls();
}
