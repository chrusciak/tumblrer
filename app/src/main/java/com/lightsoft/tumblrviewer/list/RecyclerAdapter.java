package com.lightsoft.tumblrviewer.list;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lightsoft.tumblrviewer.R;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Mateusz on 2017-02-10.
 */

public abstract class RecyclerAdapter<T> extends RecyclerView.Adapter<ViewHolder> {

    public static final int LOADING_ITEM = 0;

    private ArrayList<T> items = new ArrayList<>();

    private boolean isLoading = false;

    @Override
    public int getItemCount() {
        if (isLoading)
            return items.size() + 1;
        return items.size();
    }

    public int getDataCount() {
        return items.size();
    }

    public T getItem(int position) {
        if (position >= items.size())
            return null;
        return items.get(position);
    }

    public void clear() {
        items.clear();
    }

    public void addItems(Collection<T> items) {
        this.items.addAll(items);
    }

    public void setLoadingVisible(boolean isLoading) {
        this.isLoading = isLoading;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == LOADING_ITEM) {
            return createLoadingViewHolder(parent);
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        if (position >= items.size())
            return LOADING_ITEM;
        return super.getItemViewType(position);
    }

    @NonNull
    private ViewHolder createLoadingViewHolder(ViewGroup parent) {
        View loadingView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_loading_list, parent, false);
        return new LoadingViewHolder(loadingView);
    }
}
