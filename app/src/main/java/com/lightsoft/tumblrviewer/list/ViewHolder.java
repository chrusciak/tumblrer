package com.lightsoft.tumblrviewer.list;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.lightsoft.tumblrviewer.posts.OnOpenPostListener;

import butterknife.ButterKnife;

/**
 * Created by Mateusz on 2017-02-10.
 */

public class ViewHolder extends RecyclerView.ViewHolder {


    public ViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public View getView() {
        return itemView;
    }

}
