package com.lightsoft.tumblrviewer.list;

/**
 * Created by Mateusz on 2017-02-10.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}
