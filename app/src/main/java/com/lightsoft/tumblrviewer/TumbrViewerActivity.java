package com.lightsoft.tumblrviewer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.lightsoft.tumblrviewer.posts.PostsFragment;
import com.lightsoft.tumblrviewer.start.StartFragment;


public class TumbrViewerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tumbr_viewer);
        StartFragment startFragment = StartFragment.createInstance();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content, startFragment)
                .commit();
    }
}
