package com.lightsoft.tumblrviewer.application.component;

import com.google.gson.Gson;
import com.lightsoft.tumblrviewer.network.TumblrApiKeyInterceptor;
import com.lightsoft.tumblrviewer.network.TumblrJSRemovalInterceptor;
import com.lightsoft.tumblrviewer.network.TumblrService;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Mateusz on 2017-02-08.
 */
@Module(includes = GsonModule.class)
public class NetworkModule {

    @Provides
    @TumblrApplicationScope
    OkHttpClient okHttpClient() {
        OkHttpClient.Builder httpBuilder = new OkHttpClient.Builder();
        httpBuilder.addInterceptor(new TumblrApiKeyInterceptor());
        httpBuilder.addInterceptor(new TumblrJSRemovalInterceptor());
        return httpBuilder.build();
    }
}
