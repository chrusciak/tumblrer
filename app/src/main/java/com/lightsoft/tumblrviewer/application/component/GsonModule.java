package com.lightsoft.tumblrviewer.application.component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lightsoft.tumblrviewer.network.gson.adapter.TumblrPostAdapter;
import com.lightsoft.tumblrviewer.network.model.post.TumblrPost;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mateusz on 2017-02-09.
 */
@Module
public class GsonModule {
    @Provides
    @TumblrApplicationScope
    Gson gson(TumblrPostAdapter tumblrPostAdapter) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(TumblrPost.class, tumblrPostAdapter);
        Gson gson = gsonBuilder.create();
        return gson;
    }
}
