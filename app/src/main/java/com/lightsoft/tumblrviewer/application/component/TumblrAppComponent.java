package com.lightsoft.tumblrviewer.application.component;

import com.google.gson.Gson;

import dagger.Component;
import okhttp3.OkHttpClient;

/**
 * Created by Mateusz on 2017-02-08.
 */
@TumblrApplicationScope
@Component(modules = {NetworkModule.class})
public interface TumblrAppComponent {
    OkHttpClient getOkHttpClient();

    Gson getGson();
}
