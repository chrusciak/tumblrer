package com.lightsoft.tumblrviewer.application;

import android.app.Activity;
import android.app.Application;

import com.lightsoft.tumblrviewer.application.component.DaggerTumblrAppComponent;
import com.lightsoft.tumblrviewer.application.component.TumblrAppComponent;

/**
 * Created by Mateusz on 2017-02-08.
 */

public class TumblrApplication extends Application {

    TumblrAppComponent tumblrAppComponent;

    public static TumblrApplication getApplication(Activity activity) {
        return (TumblrApplication) activity.getApplication();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        tumblrAppComponent = DaggerTumblrAppComponent.builder()
                .build();
    }


    public TumblrAppComponent getTumblrAppComponent() {
        return tumblrAppComponent;
    }
}
