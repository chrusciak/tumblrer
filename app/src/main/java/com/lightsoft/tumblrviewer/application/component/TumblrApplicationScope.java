package com.lightsoft.tumblrviewer.application.component;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

import okhttp3.OkHttpClient;

/**
 * Created by Mateusz on 2017-02-08.
 */
@Scope
@Retention(RetentionPolicy.CLASS)
public @interface TumblrApplicationScope {

}
