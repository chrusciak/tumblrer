package com.lightsoft.tumblrviewer.network;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;

import okhttp3.ResponseBody;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created by Mateusz on 2017-02-09.
 */
@RunWith(PowerMockRunner.class)
public class TumblrJSRemovalInterceptorTest {

    @Mock
    ResponseBody responseBody;

    TumblrJSRemovalInterceptor jsRemovalInterceptor;

    @Before
    public void setUp() throws Exception {
        jsRemovalInterceptor = new TumblrJSRemovalInterceptor();
    }

    @Test
    public void shouldRemoveJavaScriptVariableFromBody() throws IOException {
        String json = "{type: video}";
        when(responseBody.string()).thenReturn("var tumblr_api_read = " + json + ";");
        String parsedBody = jsRemovalInterceptor.removeJSVariableFromResponse(responseBody);
        assertEquals(json, parsedBody);
    }

    @Test
    public void shouldRemoveJavaScriptVariableWithMultipleWhitespacesFromBody() throws IOException {
        String json = "{type: video}";
        when(responseBody.string()).thenReturn("var tumblr_api_read   =   " + json + "     ;   ");
        String parsedBody = jsRemovalInterceptor.removeJSVariableFromResponse(responseBody);
        assertEquals(json, parsedBody);
    }

    @Test
    public void shouldRemoveJavaScriptVariableWithoutWhitespacesFromBody() throws IOException {
        String json = "{type: video}";
        when(responseBody.string()).thenReturn("var tumblr_api_read=" + json + ";");
        String parsedBody = jsRemovalInterceptor.removeJSVariableFromResponse(responseBody);
        assertEquals(json, parsedBody);
    }
}