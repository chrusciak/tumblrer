package com.lightsoft.tumblrviewer.network.gson.adapter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lightsoft.tumblrviewer.network.model.ApiRead;
import com.lightsoft.tumblrviewer.network.model.post.TumblrPost;
import com.lightsoft.tumblrviewer.network.model.post.VideoPost;

import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.io.InputStreamReader;

import static org.junit.Assert.*;

/**
 * Created by Mateusz on 2017-02-12.
 */
public class TumblrPostAdapterTest {
    Gson gson;

    @Before
    public void setUp() throws Exception {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(TumblrPost.class, new TumblrPostAdapter());
        gson = gsonBuilder.create();
    }

    @Test
    public void deserializeVideoPostType() {
        InputStream inputStream = getClass().getClassLoader()
                .getResourceAsStream("video_posts_response.json");
        InputStreamReader reader = new InputStreamReader(inputStream);
        ApiRead tumblrPost = gson.fromJson(reader, ApiRead.class);
        for(TumblrPost post : tumblrPost.posts)
        {
            if(post instanceof VideoPost) assert true;
        }
    }
}