package com.lightsoft.tumblrviewer.posts;

import com.lightsoft.tumblrviewer.posts.component.DaggerTestPostsComponent;
import com.lightsoft.tumblrviewer.posts.component.TestPostsComponent;

import org.hamcrest.Description;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import retrofit2.Callback;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

/**
 * Created by Mateusz on 2017-02-11.
 */
@RunWith(MockitoJUnitRunner.class)
public class PostsPresenterTest {

    @Mock
    private PostsView postsView;

    private PostsPresenter postsPresenter;

    private TestPostsComponent testPostsComponent;

    @Before
    public void setUp() throws Exception {
        postsPresenter = new PostsPresenter(postsView);
        testPostsComponent = DaggerTestPostsComponent.builder()
                .build();
        testPostsComponent.injectPostsFragment(postsPresenter);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void shouldLoadMoreItemsWhenListOverScroll() {
        int lastElementPosition = 10;
        postsPresenter.onListOverScroll(lastElementPosition);
        verify(testPostsComponent.getPostsDataProvider()).loadPosts(eq(10), any(Callback.class));
        verify(postsView).addData(argThat(new IsListOfGivenElements(20)));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void shouldLoadFirstItemsWhenViewCreated() {
        postsPresenter.onViewCreated();
        verify(testPostsComponent.getPostsDataProvider()).loadPosts(eq(0), any(Callback.class));
        verify(postsView).addData(argThat(new IsListOfGivenElements(20)));
    }

    @Test
    public void shouldCleanListWhenListRefreshed() {
        postsPresenter.onListRefreshed();
        verify(postsView).clearList();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void shouldLoadFirstItemsWhenListRefreshed() {
        postsPresenter.onListRefreshed();
        verify(postsView).addData(argThat(new IsListOfGivenElements(20)));
    }

    public void shouldSetMaxElementWhenDataDownloaded() {
        postsPresenter.loadMore(10);
        verify(postsView).setMaximumListItems(anyInt());
    }


    @Test
    public void shouldDisableLoadingIndicatorsWhenNewDataLoaded() {
        postsPresenter.loadMore(10);
        verify(postsView).disableLoadingIndicators();
    }

    @Test
    public void shouldCancelCallWhenViewDestroyed() {
        postsPresenter.onViewDestroyed();
        verify(testPostsComponent.getPostsDataProvider()).cancelCalls();
    }

    public static class IsListOfGivenElements extends ArgumentMatcher<List> {
        private int expectedListSize;
        private int actualListSize;

        IsListOfGivenElements(int actualListSize) {
            this.expectedListSize = actualListSize;
        }

        @Override
        public boolean matches(Object argument) {
            actualListSize = ((List) argument).size();
            return actualListSize == expectedListSize;
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("\nExpected list size: " + expectedListSize);
            description.appendText("\nActual list size: " + actualListSize);
        }
    }

}