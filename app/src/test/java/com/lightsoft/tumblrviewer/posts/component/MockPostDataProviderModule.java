package com.lightsoft.tumblrviewer.posts.component;

import com.lightsoft.tumblrviewer.posts.MockPostsDataProvider;
import com.lightsoft.tumblrviewer.posts.PostsDataProvider;

import org.mockito.Mockito;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mateusz on 2017-02-11.
 */
@Module
public class MockPostDataProviderModule {
    @Provides
    @PostsScope
    public PostsDataProvider postsDataProvider() {
        return Mockito.mock(MockPostsDataProvider.class, Mockito.CALLS_REAL_METHODS);
    }
}
