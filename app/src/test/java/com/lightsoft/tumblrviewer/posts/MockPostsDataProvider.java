package com.lightsoft.tumblrviewer.posts;

import com.lightsoft.tumblrviewer.network.model.ApiRead;
import com.lightsoft.tumblrviewer.network.model.post.TumblrPost;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mateusz on 2017-02-11.
 */

public class MockPostsDataProvider implements PostsDataProvider {
    @Override
    public void loadPosts(int offset, Callback<ApiRead> callback) {
        if (callback != null)
            callback.onResponse(null, createTumblrPostsResponse());
    }

    @Override
    public void cancelCalls() {

    }

    private Response<ApiRead> createTumblrPostsResponse() {
        List<TumblrPost> posts = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            posts.add(new TumblrPost());
        }
        ApiRead apiRead = new ApiRead();
        apiRead.posts = posts;
        apiRead.totalPosts = 60;
        return Response.success(apiRead);
    }
}
