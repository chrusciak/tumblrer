package com.lightsoft.tumblrviewer.posts.component;

import com.lightsoft.tumblrviewer.application.component.TumblrAppComponent;
import com.lightsoft.tumblrviewer.mvp.DataProvider;
import com.lightsoft.tumblrviewer.posts.PostsDataProvider;
import com.lightsoft.tumblrviewer.posts.PostsFragment;
import com.lightsoft.tumblrviewer.posts.PostsPresenter;

import dagger.Component;

/**
 * Created by Mateusz on 2017-02-08.
 */
@PostsScope
@Component(modules = MockPostDataProviderModule.class)
public interface TestPostsComponent {
    void injectPostsFragment(PostsPresenter postsPresenter);

    PostsDataProvider getPostsDataProvider();
}
