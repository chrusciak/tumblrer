package com.lightsoft.tumblrviewer.start;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

/**
 * Created by Mateusz on 2017-02-15.
 */
@RunWith(MockitoJUnitRunner.class)
public class StartPresenterTest {
    @Mock
    private StartView startView;

    private StartPresenter startPresenter;


    @Before
    public void setUp() throws Exception {
        startPresenter = new StartPresenter(startView);
    }

    @Test
    public void shouldOpenBlogForUserWhenUserClickGoAndUserIsNotEmpty()
    {
        String username = "robertmagier";
        startPresenter.onGoButtonClicked(username);
        verify(startView).openBlogForUser(username);
    }

    @Test
    public void shouldNotOpenBlogForUserWhenUserClickGoAndUserIsNotEmpty()
    {
        String username = "";
        startPresenter.onGoButtonClicked(username);
        verify(startView, never()).openBlogForUser(username);
    }
}